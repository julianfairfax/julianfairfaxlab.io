[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# My Website

## Licensing information
The [FreeSans](https://www.gnu.org/software/freefont/) font (GPL3) is under the license of its developers. This repository's license only applies to the code for the repository itself.

If you believe your code has been unjustly used or has been used without proper credit in this repository, please [contact](https://julianfairfax.ch/contact) me.

## Self-hosting information
If you are going to host a site like this yourself, it is requested that you remove my content from it and replace it with your own. This includes references to my name, projects, or website.

You must remove all folders except the `_layouts`, `_includes`, and `assets` folders and all files except the `_config.yml`, `LICENSE.txt`, and `README.md` files.

You must also modify the `_layouts/default.html`, `_config.yml`, and `README.md` files to remove my name.

You will need to add your own files for your website to have any content. Creating an `index.md` or `index.html` file will create a home page.

You will also need to modify the `_layouts/default.html` file to include your pages in the navigation section.

Under the terms of the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/) license used by this project, you are required to use this same license in any and all redistributions of this project's code.

This site one is made to be compatible with GitHub Pages, GitLab Pages, and Codeberg Pages.
