---
title: Linux makes Macs better
description: 29 October 2022
layout: default
---

Throughout the past few years, I have developed and helped with the development of tools to run newer versions of macOS on older Macs that Apple dropped support for.

I learnt a lot of things by doing that, and I enjoyed the time I spent developing the tools and interacting with the community. However, I always knew that there was something better for these Macs.

Linux is not for everyone, that's for sure. But when you have a Mac that can either use an old version of macOS without app support, or a newer version of macOS without hardware acceleration support, neither is a good option.

This is the case for the MacBook4,1 I have. My macOS Patcher tool is the only one to support installing macOS versions 10.12 to 10.15 on the MacBook4,1. But it doesn't support hardware acceleration on any of these versions.

This is because the graphics driver used by macOS is 32-bit, and those haven't been supported by macOS for eleven years. In contrast, the latest version of Arch Linux runs without issue on my MacBook.

It's of course slower than it is on my laptop, but much faster than macOS is, because it has hardware acceleration through the official open source graphics driver.

It does require installing two non-free pieces of firmware. One for the Bluetooth card, and one for the camera.

They use non-free pieces of firmware, but the drivers for these devices are still open source and are part of the Linux kernel. They also work perfectly, as does the rest of the operating system. If you're on one of these Macs, Linux really is your best bet.
