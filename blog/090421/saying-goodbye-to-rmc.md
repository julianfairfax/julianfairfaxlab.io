---
title: Saying Goodbye to RMC
description: 9 April 2021
layout: default
---

It's been two years now since I started RMC, the "team of people around the world who have a passion for computing and the skills to research, modify, and customize many parts of the computing experience" with "members who are skilled with all different kinds of computer hardware and software". I really enjoyed working on each any every one of the projects that we released but it's just that, we were never really a team.

I had achieved my goal of connecting with, and even becoming friends with people who definitely did have a passion for computing, and definitely were skilled, but all of the projects that RMC has released since it's start two years ago have been exclusively worked on by me, and the occasional outside contributor. This is not to say that RMC's members didn't work anything, they just didn't work on any of RMC's projects.

There's also another aspect to this which is that I, the developer of macOS Patcher switched to Linux a few months ago and that brought an end to support for macOS Patcher and many other projects that RMC developed. We also lost most of the community we had as a result of this but even before as a result of the newer macOS versions being a challenge we didn't take on for the community of macOS Patcher, and it being an community I wasn't part of anymore.

Saying goodbye to RMC doesn't mean I'm done with developing software. In fact I have got a few projects that I have already released that support Linux and that I'm actively working on, and there will be more. It just means that I'm recognising that this work belongs to me and that the work I'm doing now, is no longer of interest to the community I started, and have mostly lost, with RMC.

With all that being said, RMC's repositories and website content have been moved to [my own account](https://gitlab.com/julianfairfax) and website and profiles, forums, and threads have been updated with links to my own repositories and website. To our members and to our community, thank you for being a part of this while it was around and I hope you'll still be a part of all the future work I'm going to be involved in. Goodbye RMC
