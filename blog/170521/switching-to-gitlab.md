---
title: Switching to GitLab
description: 17 May 2021 
layout: default
---

This is just a small blog post to announce that I'm switching to GitLab for privacy and open source reasons; GitHub is owned by Microsoft and while it may be built for the open source community, it is not built by the open source community. I would rather use a tool that is not owned by a big tech company and open source. This way I can make sure they respect my privacy, and could even self-host it one day.

My projects have all been moved to [GitLab](https://gitlab.com/julianfairfax) thanks to the built-in migration tool it has and my website now redirects to the GitLab version. I will still keep my GitHub account for contributing to open source projects on their platform but I won't be uploading my own projects onto it anymore. Hopefully this change won't cause too many issues.

I will also try to have the links to my repositories updated on platforms where they're mentioned but that is out of my control since I don't have an account on most of them.
