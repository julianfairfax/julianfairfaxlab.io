---
title: A new computer
description: 18 March 2021
layout: default
---

As you may or may not know, I have been using a MacBook7,1 for the past 3 years. During that time I was a member and a developer in the macOS on Unsupported Macs community, but on 20 January 2021, I officially switched to running Linux (I had been running it before then) and thus left the macOS on Unsupported Macs community. The thing is, whether it was on a supported version of macOS, an unsupported version of macOS, or even Linux, the performance of such an old computer was beginning to bother me. All of this led to me to make the decision to buy a new computer.

I wanted something not only capable of running Linux, but something that would be good at it. I looked through a few different options and eventually settled on the [15-inch TUXEDO Pulse 15](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/TUXEDO-Book-Pulse-15-Gen1.tuxedo) for a few reasons: it had modern specs, a modern design, and a good company behind it that I could trust to deliver on my expectations. I got it a bit under a week ago and have been using it since then. 

I think the best review I can give of having a new computer is that it lets me run the same software, and do the same work, but I can run that stuff and do that work so much faster, which makes it a much more enjoyable experience. Another thing that appealed to me was that all the drivers for it were open source and I didn't have to install any additional drivers.

So far I have really enjoyed using this computer and I cannot recommend it enough for someone who wants a capable computer for Linux with a guarantee that all the hardware will work. The specs of my model are the following, in case you're interested:
- AMD Ryzen 7 4800H CPU
- AMD Radeon RX Vega 7 CPU
- 8GB/1 x 8GB 3200Mhz CL22 Samsung RAM (you can upgrade this!)
- 250GB Samsung 860 EVO SSD (you can upgrade this!)
- Intel Wi-Fi 6 AX200
- Bluetooth 5.1
