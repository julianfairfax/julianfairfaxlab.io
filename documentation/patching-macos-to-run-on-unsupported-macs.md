---
title: Patching macOS to run on unsupported Macs
description: Detailed information on the patches required to run macOS on unsupported Macs.
layout: default
---

## What is an unsupported Mac?
A Mac becomes unsupported when Apple’s latest operating system no longer runs on it, without our patches, of course.

## Why should I run a patched version of macOS on my unsupported Mac?
A newer version of macOS means you’ll have better security, and better support for the apps you want to use.

## Why should I learn how to patch macOS?
If you use an automatic patcher tool, you won’t have to do any work by yourself in order to patch macOS, but it’s still cool to know what your tool is doing, isn’t it? If you’d like to develop your own patcher tool, this writeup is a good place to start!


# OS X 10.8 Mountain Lion

## What Macs became unsupported with this version’s release?

- iMac5,1
- iMac5,2
- iMac6,1
- MacBook2,1
- MacBook3,1
- MacBook4,1
- MacBookAir1,1
- MacBookPro2,1
- MacBookPro2,2
- Macmini2,1
- MacPro1,1
- MacPro2,1
- Xserve1,1
- Xserve2,1

## The 32-bit driver problem
Mountain Lion has a 64-bit kernel and no longer supports loading 32-bit drivers. This isn’t a problem for ATI cards since they have 64-bit drivers.

Mountain Lion beta 1 had a 32-bit kernel, which you can use on newer versions of Mountain Lion. However, this also requires replacing a bunch of system components with 32-bit copies from the beta. This method supports graphics acceleration on all Macs. With the stock kernel, you can only achieve graphics acceleration on ATI cards.

If you want to achieve graphics acceleration on all Macs, you have to remove these files:
- AppleCameraInterface.kext (in /System/Library/Extensions/)
- AppleIntelLpssDmac.kext (in /System/Library/Extensions/)
- AppleIntelLpssGspi.kext (in /System/Library/Extensions/)
- AppleIntelLpssSpiController.kext (in /System/Library/Extensions/)
- AppleHSSPIHIDDriver.kext (in /System/Library/Extensions/)
- AppleHSSPISupport.kext (in /System/Library/Extensions/)
- AppleTopCase.kext (in /System/Library/Extensions/)
- AppleUSBEthernetHost.kext (in /System/Library/Extensions/)
- vecLib.kext (in /System/Library/Extensions/)
- corecrypto.kext (in /System/Library/Extensions/)
- IOUSBFamily.kext/Contents/PlugIns/AppleUSBXHCI.kext (in /System/Library/Extensions/)
- ATTOExpressSASHBA3.kext (in /System/Library/Extensions/)
- AirPortUserAgent.plugin (in /System/Library/UserEventPlugins/)
- Apple80211Monitor.bundle (in /System/Library/SystemConfiguration/)
- EAPOLController.bundle (in /System/Library/SystemConfiguration/)

You have to replace these files with older versions from 10.8 beta 1 (these can be obtained from [this copy](https://archive.org/details/osx108dp1) of OS X 10.8 Mountain Lion beta 1 from the Internet Archive):
- AVRCPAgent.app (in /System/Library/CoreServices/)
- BluetoothAudioAgent.app (in /System/Library/CoreServices/)
- Bluetooth Setup Assistant.app (in /System/Library/CoreServices/)
- Bluetooth.menu (in /System/Library/CoreServices/Menu Extras/)
- Accusys6xxxx.kext (in /System/Library/Extensions/)
- acfs.kext (in /System/Library/Extensions/)
- acfsctl.kext (in /System/Library/Extensions/)
- ALF.kext (in /System/Library/Extensions/)
- Apple16X50Serial.kext (in /System/Library/Extensions/)
- AppleACPIPlatform.kext (in /System/Library/Extensions/)
- AppleAHCIPort.kext (in /System/Library/Extensions/)
- AppleAPIC.kext (in /System/Library/Extensions/)
- AppleAVBAudio.kext (in /System/Library/Extensions/)
- AppleBacklight.kext (in /System/Library/Extensions/)
- AppleBacklightExpert.kext (in /System/Library/Extensions/)
- AppleBluetoothMultitouch.kext (in /System/Library/Extensions/)
- AppleBMC.kext (in /System/Library/Extensions/)
- AppleEFIRuntime.kext (in /System/Library/Extensions/)
- AppleFileSystemDriver.kext (in /System/Library/Extensions/)
- AppleFSCompressionTypeDataless.kext (in /System/Library/Extensions/)
- AppleFSCompressionTypeZlib.kext (in /System/Library/Extensions/)
- AppleFWAudio.kext (in /System/Library/Extensions/)
- AppleGraphicsControl.kext (in /System/Library/Extensions/)
- AppleGraphicsPowerManagement.kext (in /System/Library/Extensions/)
- AppleHIDKeyboard.kext (in /System/Library/Extensions/)
- AppleHIDMouse.kext (in /System/Library/Extensions/)
- AppleHPET.kext (in /System/Library/Extensions/)
- AppleHWSensor.kext (in /System/Library/Extensions/)
- AppleIntelCPUPowerManagement.kext (in /System/Library/Extensions/)
- AppleIntelCPUPowerManagementClient.kext (in /System/Library/Extensions/)
- AppleIntelFramebufferCapri.kext (in /System/Library/Extensions/)
- AppleIntelHD3000Graphics.kext (in /System/Library/Extensions/)
- AppleIntelHD3000GraphicsGA.plugin (in /System/Library/Extensions/)
- AppleIntelHD3000GraphicsGLDriver.bundle (in /System/Library/Extensions/)
- AppleIntelHD3000GraphicsVADriver.bundle (in /System/Library/Extensions/)
- AppleIntelHD4000Graphics.kext (in /System/Library/Extensions/)
- AppleIntelHD4000GraphicsGA.plugin (in /System/Library/Extensions/)
- AppleIntelHD4000GraphicsGLDriver.bundle (in /System/Library/Extensions/)
- AppleIntelHD4000GraphicsVADriver.bundle (in /System/Library/Extensions/)
- AppleIntelHDGraphics.kext (in /System/Library/Extensions/)
- AppleIntelHDGraphicsFB.kext (in /System/Library/Extensions/)
- AppleIntelHDGraphicsGA.plugin (in /System/Library/Extensions/)
- AppleIntelHDGraphicsGLDriver.bundle (in /System/Library/Extensions/)
- AppleIntelHDGraphicsVADriver.bundle (in /System/Library/Extensions/)
- AppleIntelIVBVA.bundle (in /System/Library/Extensions/)
- AppleIntelSNBGraphicsFB.kext (in /System/Library/Extensions/)
- AppleIntelSNBVA.bundle (in /System/Library/Extensions/)
- AppleIRController.kext (in /System/Library/Extensions/)
- Apple_iSight.kext (in /System/Library/Extensions/)
- AppleKeyStore.kext (in /System/Library/Extensions/)
- AppleKeyswitch.kext (in /System/Library/Extensions/)
- AppleLPC.kext (in /System/Library/Extensions/)
- AppleLSIFusionMPT.kext (in /System/Library/Extensions/)
- AppleMatch.kext (in /System/Library/Extensions/)
- AppleMCCSControl.kext (in /System/Library/Extensions/)
- AppleMCEDriver.kext (in /System/Library/Extensions/)
- AppleMCP89RootPortPM.kext (in /System/Library/Extensions/)
- AppleMIDIFWDriver.plugin (in /System/Library/Extensions/)
- AppleMIDIIACDriver.plugin (in /System/Library/Extensions/)
- AppleMIDIRTPDriver.plugin (in /System/Library/Extensions/)
- AppleMIDIUSBDriver.plugin (in /System/Library/Extensions/)
- AppleMikeyHIDDriver.kext (in /System/Library/Extensions/)
- AppleMultitouchDriver.kext (in /System/Library/Extensions/)
- ApplePlatformEnabler.kext (in /System/Library/Extensions/)
- AppleRAID.kext (in /System/Library/Extensions/)
- AppleRAIDCard.kext (in /System/Library/Extensions/)
- AppleRTC.kext (in /System/Library/Extensions/)
- AppleSDXC.kext (in /System/Library/Extensions/)
- AppleSEP.kext (in /System/Library/Extensions/)
- AppleSmartBatteryManager.kext (in /System/Library/Extensions/)
- AppleSMBIOS.kext (in /System/Library/Extensions/)
- AppleSMBusController.kext (in /System/Library/Extensions/)
- AppleSMBusPCI.kext (in /System/Library/Extensions/)
- AppleSMC.kext (in /System/Library/Extensions/)
- AppleSMCLMU.kext (in /System/Library/Extensions/)
- AppleSRP.kext (in /System/Library/Extensions/)
- AppleStorageDrivers.kext (in /System/Library/Extensions/)
- AppleThunderboltDPAdapters.kext (in /System/Library/Extensions/)
- AppleThunderboltEDMService.kext (in /System/Library/Extensions/)
- AppleThunderboltNHI.kext (in /System/Library/Extensions/)
- AppleThunderboltPCIAdapters.kext (in /System/Library/Extensions/)
- AppleThunderboltUTDM.kext (in /System/Library/Extensions/)
- AppleTyMCEDriver.kext (in /System/Library/Extensions/)
- AppleUpstreamUserClient.kext (in /System/Library/Extensions/)
- AppleUSBAudio.kext (in /System/Library/Extensions/)
- AppleUSBDisplays.kext (in /System/Library/Extensions/)
- AppleUSBEthernetHost_32.kext (in /System/Library/Extensions/)
- AppleUSBMultitouch.kext (in /System/Library/Extensions/)
- AppleUSBTopCase.kext (in /System/Library/Extensions/)
- AppleVADriver.bundle (in /System/Library/Extensions/)
- AppleWWANAutoEject.kext (in /System/Library/Extensions/)
- AppleXsanFilter.kext (in /System/Library/Extensions/)
- ArcMSR.kext (in /System/Library/Extensions/)
- ATI2400Controller.kext (in /System/Library/Extensions/)
- ATI2600Controller.kext (in /System/Library/Extensions/)
- ATI3800Controller.kext (in /System/Library/Extensions/)
- ATI4600Controller.kext (in /System/Library/Extensions/)
- ATI4800Controller.kext (in /System/Library/Extensions/)
- ATI5000Controller.kext (in /System/Library/Extensions/)
- ATI6000Controller.kext (in /System/Library/Extensions/)
- ATIRadeonX2000.kext (in /System/Library/Extensions/)
- ATIRadeonX2000GA.plugin (in /System/Library/Extensions/)
- ATIRadeonX2000GLDriver.bundle (in /System/Library/Extensions/)
- ATIRadeonX2000VADriver.bundle (in /System/Library/Extensions/)
- ATIRadeonX3000.kext (in /System/Library/Extensions/)
- ATIRadeonX3000GA.plugin (in /System/Library/Extensions/)
- ATIRadeonX3000GLDriver.bundle (in /System/Library/Extensions/)
- ATIRadeonX3000VADriver.bundle (in /System/Library/Extensions/)
- ATTOCelerityFC.kext (in /System/Library/Extensions/)
- ATTOCelerityFC8.kext (in /System/Library/Extensions/)
- ATTOExpressPCI4.kext (in /System/Library/Extensions/)
- ATTOExpressSASHBA.kext (in /System/Library/Extensions/)
- ATTOExpressSASHBA2.kext (in /System/Library/Extensions/)
- ATTOExpressSASRAID.kext (in /System/Library/Extensions/)
- ATTOExpressSASRAID2.kext (in /System/Library/Extensions/)
- AudioAUUC.kext (in /System/Library/Extensions/)
- autofs.kext (in /System/Library/Extensions/)
- BJUSBLoad.kext (in /System/Library/Extensions/)
- BootCache.kext (in /System/Library/Extensions/)
- CalDigitHDProDrv.kext (in /System/Library/Extensions/)
- cd9660.kext (in /System/Library/Extensions/)
- cddafs.kext (in /System/Library/Extensions/)
- CellPhoneHelper.kext (in /System/Library/Extensions/)
- CoreStorage.kext (in /System/Library/Extensions/)
- Dont Steal Mac OS X.kext (in /System/Library/Extensions/)
- DSACL.ppp (in /System/Library/Extensions/)
- DSAuth.ppp (in /System/Library/Extensions/)
- DVFamily.bundle (in /System/Library/Extensions/)
- EAP-KRB.ppp (in /System/Library/Extensions/)
- EAP-RSA.ppp (in /System/Library/Extensions/)
- EAP-TLS.ppp (in /System/Library/Extensions/)
- EPSONUSBPrintClass.kext (in /System/Library/Extensions/)
- exfat.kext (in /System/Library/Extensions/)
- GeForce7xxx.kext (in /System/Library/Extensions/)
- GeForce7xxxGA.plugin (in /System/Library/Extensions/)
- GeForce7xxxVADriver.bundle (in /System/Library/Extensions/)
- GeForceGLDriver.bundle (in /System/Library/Extensions/)
- HighPointIOP.kext (in /System/Library/Extensions/)
- HighPointRR.kext (in /System/Library/Extensions/)
- HighPointRR644.kext (in /System/Library/Extensions/)
- hp_fax_io.kext (in /System/Library/Extensions/)
- hp_Inkjet1_io_enabler.kext (in /System/Library/Extensions/)
- IO80211Family.kext (in /System/Library/Extensions/)
- IOAccelerator2D.plugin (in /System/Library/Extensions/)
- IOAcceleratorFamily.kext (in /System/Library/Extensions/)
- IOACPIFamily.kext (in /System/Library/Extensions/)
- IOAHCIFamily.kext (in /System/Library/Extensions/)
- IOATAFamily.kext (in /System/Library/Extensions/)
- IOAVBFamily.kext (in /System/Library/Extensions/)
- IOBDStorageFamily.kext (in /System/Library/Extensions/)
- IOBluetoothFamily.kext (in /System/Library/Extensions/)
- IOBluetoothHIDDriver.kext (in /System/Library/Extensions/)
- IOCDStorageFamily.kext (in /System/Library/Extensions/)
- IODVDStorageFamily.kext (in /System/Library/Extensions/)
- IOFireWireAVC.kext (in /System/Library/Extensions/)
- IOFireWireFamily.kext (in /System/Library/Extensions/)
- IOFireWireIP.kext (in /System/Library/Extensions/)
- IOFireWireSBP2.kext (in /System/Library/Extensions/)
- IOFireWireSerialBusProtocolTransport.kext (in /System/Library/Extensions/)
- IOGraphicsFamily.kext (in /System/Library/Extensions/)
- IOHDIXController.kext (in /System/Library/Extensions/)
- IOHIDFamily.kext (in /System/Library/Extensions/)
- IONDRVSupport.kext (in /System/Library/Extensions/)
- IONetworkingFamily.kext (in /System/Library/Extensions/)
- IOPCIFamily.kext (in /System/Library/Extensions/)
- IOPlatformPluginFamily.kext (in /System/Library/Extensions/)
- IOSCSIArchitectureModelFamily.kext (in /System/Library/Extensions/)
- IOSCSIParallelFamily.kext (in /System/Library/Extensions/)
- IOSerialFamily.kext (in /System/Library/Extensions/)
- IOSMBusFamily.kext (in /System/Library/Extensions/)
- IOStorageFamily.kext (in /System/Library/Extensions/)
- IOStreamFamily.kext (in /System/Library/Extensions/)
- IOSurface.kext (in /System/Library/Extensions/)
- IOThunderboltFamily.kext (in /System/Library/Extensions/)
- IOTimeSyncFamily.kext (in /System/Library/Extensions/)
- IOUSBAttachedSCSI.kext (in /System/Library/Extensions/)
- IOUSBFamily.kext (in /System/Library/Extensions/)
- IOUSBMassStorageClass.kext (in /System/Library/Extensions/)
- IOUserEthernet.kext (in /System/Library/Extensions/)
- IOVideoFamily.kext (in /System/Library/Extensions/)
- iPodDriver.kext (in /System/Library/Extensions/)
- JMicronATA.kext (in /System/Library/Extensions/)
- L2TP.ppp (in /System/Library/Extensions/)
- mcxalr.kext (in /System/Library/Extensions/)
- msdosfs.kext (in /System/Library/Extensions/)
- ntfs.kext (in /System/Library/Extensions/)
- NVDAGF100Hal.kext (in /System/Library/Extensions/)
- NVDAGK100Hal.kext (in /System/Library/Extensions/)
- NVDANV40HalG7xxx.kext (in /System/Library/Extensions/)
- NVDAResmanG7xxx.kext (in /System/Library/Extensions/)
- NVSMU.kext (in /System/Library/Extensions/)
- OSvKernDSPLib.kext (in /System/Library/Extensions/)
- PPP.kext (in /System/Library/Extensions/)
- PPPoE.ppp (in /System/Library/Extensions/)
- PPPSerial.ppp (in /System/Library/Extensions/)
- PPTP.ppp (in /System/Library/Extensions/)
- PromiseSTEX.kext (in /System/Library/Extensions/)
- Quarantine.kext (in /System/Library/Extensions/)
- Radius.ppp (in /System/Library/Extensions/)
- SM56KUSBAudioFamily.kext (in /System/Library/Extensions/)
- SMARTLib.plugin (in /System/Library/Extensions/)
- smbfs.kext (in /System/Library/Extensions/)
- SMCMotionSensor.kext (in /System/Library/Extensions/)
- SoftRAID.kext (in /System/Library/Extensions/)
- System.kext (in /System/Library/Extensions/)
- TMSafetyNet.kext (in /System/Library/Extensions/)
- triggers.kext (in /System/Library/Extensions/)
- udf.kext (in /System/Library/Extensions/)
- webcontentfilter.kext (in /System/Library/Extensions/)
- webdav_fs.kext (in /System/Library/Extensions/)
- afpfs.kext (in /System/Library/Filesystems/AppleShare/)
- asp_tcp.kext (in /System/Library/Filesystems/AppleShare/)
- hfs.fs (in /System/Library/Filesystems/)
- afp.bundle (in /System/Library/Filesystems/NetFSPlugins/)
- AppleShareClientCore.framework (in /System/Library/Frameworks/)
- CoreWiFi.framework (in /System/Library/Frameworks/)
- CoreWLAN.framework (in /System/Library/Frameworks/)
- IOBluetooth.framework (in /System/Library/Frameworks/)
- NetFS.framework (in /System/Library/Frameworks/)
- Bluetooth.prefPane (in /System/Library/PreferencePanes/)
- Mouse.prefPane (in /System/Library/PreferencePanes/)
- Apple80211.framework (in /System/Library/PrivateFrameworks/)
- AppleProfileFamily.framework (in /System/Library/PrivateFrameworks/)
- CoreWLANKit.framework (in /System/Library/PrivateFrameworks/)
- EAP8021X.framework (in /System/Library/PrivateFrameworks/)
- Apple80211Monitor.bundle (in /System/Library/SystemConfiguration/)
- EAPOLController.bundle (in /System/Library/SystemConfiguration/)
- SPBluetoothReporter.spreporter (in /System/Library/SystemProfiler/)
- SPDisplaysReporter.spreporter (in /System/Library/SystemProfiler/)
- AirPortUserAgent.plugin (in /System/Library/UserEventPlugins/)
- BluetoothUserAgent-Plugin.plugin (in /System/Library/UserEventPlugins/)
- airportd (in /usr/libexec/)
- wifid (in /usr/libexec/)
- wps (in /usr/libexec/)
- blued (in /usr/sbin/)
- bnepd (in /usr/sbin/)
- mDNSResponder (in /usr/sbin/)
- mach_kernel (in /)

You have to replace these with older versions from 10.7.5:
- AppleIntelGMA950.kext (in /System/Library/Extensions)
- AppleIntelGMA950GA.plugin (in /System/Library/Extensions)
- AppleIntelGMA950GLDriver.bundle (in /System/Library/Extensions)
- AppleIntelGMA950VADriver.bundle (in /System/Library/Extensions)
- AppleIntelGMAX3100.kext (in /System/Library/Extensions)
- AppleIntelGMAX3100FB.kext (in /System/Library/Extensions)
- AppleIntelGMAX3100GA.plugin (in /System/Library/Extensions)
- AppleIntelGMAX3100GLDriver.bundle (in /System/Library/Extensions)
- AppleIntelGMAX3100VADriver.bundle (in /System/Library/Extensions)
- AppleIntelIntegratedFramebuffer.kext (in /System/Library/Extensions)
- GeForce.kext (in /System/Library/Extensions)
- GeForce7xxx.kext (in /System/Library/Extensions)
- GeForce7xxxGA.plugin (in /System/Library/Extensions)
- GeForce7xxxGLDriver.bundle (in /System/Library/Extensions)
- GeForce7xxxVADriver.bundle (in /System/Library/Extensions)
- GeForceGA.plugin (in /System/Library/Extensions)
- GeForceGLDriver.bundle (in /System/Library/Extensions)
- GeForceVADriver.bundle (in /System/Library/Extensions)
- NVDAGF100Hal.kext (in /System/Library/Extensions)
- NVDAGK100Hal.kext (in /System/Library/Extensions)
- NVDANV40HalG7xxx.kext (in /System/Library/Extensions)
- NVDANV50Hal.kext (in /System/Library/Extensions)
- NVDAResman.kext (in /System/Library/Extensions)
- NVDAResmanG7xxx.kext (in /System/Library/Extensions)

You have to replace these files with modified versions from parrotgeek1:
- OpenCL.framework (in /System/Library/Frameworks/)
- OpenGL.framework (in /System/Library/Frameworks/)

You should also replace this file with a modified version:
- com.apple.Dock.plist (in /System/Library/LaunchAgents/)

Specifically, to load this custom file from parrotgeek1, which you should also add:
- gmacheck (in /System/Library/CoreServices/)

If you want to achieve graphics acceleration on ATI Macs, you only have to replace these files with modified versions from parrotgeek1:
- OpenCL.framework (in /System/Library/Frameworks/)
- OpenGL.framework (in /System/Library/Frameworks/)

## The 32-bit firmware problem
Most older Intel Macs have 64-bit CPUs but still use 32-bit firmware, so they can’t load a 64-bit kernel. The MacBook3,1 and MacBook4,1 both have 64-bit firmware but still try to load a 32-bit kernel. The best solution is to use the official file from Yosemite, which seems to work without patches!

If you compile Apple’s boot.efi yourself, you can add support for Macs with 32-bit firmware. Due to the efforts of Piker Alpha, you can download pre-compiled copies from patchers. I used andyvand's efilipo to combine the compiled 32-bit copy and Yosemite file into one single boot.efi file which supports all unsupported Macs.

## What projects can I use/browse for more information?
parrotgeek1 released NexPostFacto 10.8 32-bit for running Mountain Lion on unsupported Macs and NexPostFacto 10.8 64-bit for unsupported Macs with ATI cards.

I also released a tool of my own, OS X Patcher, which uses parrotgeek1's acceleration patches to provide the acceleration support.

## What new patches do I have to use for this version?
Delete all ATI and AMD drivers before replacing them, otherwise your system won’t boot.

If you don't want to achieve graphics acceleration, you have to replace these with older versions from 10.6.2 beta 1 (these can be obtained from [these copies](https://archive.org/details/10a222) of Mac OS X 10.8 Snow Leopard betas from the Internet Archive):
- AppleIntelGMA950.kext (in /System/Library/Extensions)
- AppleIntelGMA950GA.plugin (in /System/Library/Extensions)
- AppleIntelGMA950GLDriver.bundle (in /System/Library/Extensions)
- AppleIntelGMA950VADriver.bundle (in /System/Library/Extensions)
- AppleIntelGMAX3100.kext (in /System/Library/Extensions)
- AppleIntelGMAX3100FB.kext (in /System/Library/Extensions)
- AppleIntelGMAX3100GA.plugin (in /System/Library/Extensions)
- AppleIntelGMAX3100GLDriver.bundle (in /System/Library/Extensions)
- AppleIntelGMAX3100VADriver.bundle (in /System/Library/Extensions)
- AppleIntelIntegratedFramebuffer.kext (in /System/Library/Extensions)
- GeForce.kext (in /System/Library/Extensions)
- GeForce7xxxGLDriver.bundle (in /System/Library/Extensions)
- GeForce8xxxGLDriver.bundle (in /System/Library/Extensions)
- GeForceGA.plugin (in /System/Library/Extensions)
- GeForceVADriver.bundle (in /System/Library/Extensions)
- NVDANV40Hal.kext (in /System/Library/Extensions)
- NVDANV50Hal.kext (in /System/Library/Extensions)
- NVDAResman.kext (in /System/Library/Extensions)

You have to replace these with older versions from 10.7.5:
- AppleHDA.kext (in /System/Library/Extensions)
- ATI1300Controller.kext (in /System/Library/Extensions)
- ATI1600Controller.kext (in /System/Library/Extensions)
- ATI1900Controller.kext (in /System/Library/Extensions)
- ATIFramebuffer.kext (in /System/Library/Extensions)
- ATIRadeonX1000.kext (in /System/Library/Extensions)
- ATIRadeonX1000GA.plugin (in /System/Library/Extensions)
- ATIRadeonX1000GLDriver.bundle (in /System/Library/Extensions)
- ATIRadeonX1000VADriver.bundle (in /System/Library/Extensions)
- ATISupport.kext (in /System/Library/Extensions)

You have to replace this one with an older version from 10.8.4:
- IOAudioFamily.kext (in /System/Library/Extensions)

If you're using a Macmini1,1 or Macmini2,1 and you haven't already replaced these files, you have to replace this one with an older version from 10.8.3:
- airportd (in /usr/libexec)

And you have to replace this one with an older version from 10.8 beta 1 (this can be obtained from [this copy](https://archive.org/details/osx108dp1) of OS X 10.8 Mountain Lion beta 1 from the Internet Archive):
- IO80211Family.kext (in /System/Library/Extensions)

# OS X 10.9 Mavericks

## The 32-bit driver problem
Mavericks has a 64-bit kernel and no longer supports loading 32-bit drivers. This isn’t a problem for ATI cards since they have 64-bit drivers.

Mountain Lion beta 1 had a 32-bit kernel, which you can use on newer versions of Mountain Lion. However, this also requires replacing a bunch of system components with 32-bit copies from the beta. This method supports graphics acceleration on all Macs. On Mavericks, you can only achieve graphics acceleration on ATI cards.

If you want to achieve graphics acceleration on ATI Macs, you have to replace these files with modified versions from parrotgeek1:
- OpenCL.framework (in /System/Library/Frameworks/)
- OpenGL.framework (in /System/Library/Frameworks/)
- CoreGraphics.framework (in /System/Library/Frameworks/)
- QuartzCore.framework (in /System/Library/Frameworks/)
- Slideshows.framework (in /System/Library/PrivateFrameworks/)

## What projects can I use/browse for more information?
parrotgeek1 recently released NexPostFacto 10.9 for running Mavericks on unsupported Macs with ATI cards.

I also released a tool of my own, OS X Patcher, which uses parrotgeek1's acceleration patches to provide the acceleration support.


# OS X 10.10 Yosemite

## What projects can I use/browse for more information?
Acceleration may be possible on unsupported Macs with ATI cards but no tool has been released.

I released a tool of my own, OS X Patcher, which doesn’t support graphics acceleration, but it’s still useful for some!


# OS X 10.11 El Capitan

## The 32-bit firmware problem
Most older Intel Macs have 64-bit CPUs but still use 32-bit firmware, so they can’t load a 64-bit kernel. El Capitan updated the way kernel cache is handled, and thus needs a recompiled boot.efi file for Macs with 32-bit firmware.

If you compile Apple’s boot.efi yourself, you can add support for Macs with 32-bit firmware. Due to the efforts of Piker Alpha, you can download pre-compiled copies from patchers. I used andyvand's efilipo to combine the recompiled 32-bit copy and stock file into one single boot.efi file which supports all unsupported Macs.

## What projects can I use/browse for more information?
Acceleration may be possible on unsupported Macs with ATI cards but no tool has been released.

I released a tool of my own, OS X Patcher, which doesn’t support graphics acceleration, but it’s still useful for some!

## What new patches do I have to use for this version?
El Capitan updated drivers for input devices such as the mouse and keyboard, you have to replace these with older versions from 10.10.5:
- AppleHIDMouse.kext (in /System/Library/Extensions)
- AppleIRController.kext (in /System/Library/Extensions)
- AppleTopCase.kext (in /System/Library/Extensions)
- AppleUSBMultitouch.kext (in /System/Library/Extensions)
- AppleUSBTopCase.kext (in /System/Library/Extensions)
- IOBDStorageFamily.kext (in /System/Library/Extensions)
- IOBluetoothFamily.kext (in /System/Library/Extensions)
- IOBluetoothHIDDriver.kext (in /System/Library/Extensions)
- IOSerialFamily.kext (in /System/Library/Extensions)
- IOUSBFamily.kext (in /System/Library/Extensions)
- IOUSBMassStorageClass.kext (in /System/Library/Extensions)

And this one with a modified version of the stock 10.11.6 copy:
- IOUSBHostFamily.kext (in /System/Library/Extensions)

Specifically, remove the following files from Contents/PlugIns:
- AppleUSBEHCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)
- AppleUSBEHCIPCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)
- AppleUSBOHCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)
- AppleUSBOHCIPCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)
- AppleUSBUHCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)
- AppleUSBUHCIPCI.kext (in /System/Library/Extensions/IOUSBHostFamily.kext/Contents/PlugIns)

## What new steps do I have to use for this version?
El Capitan updated the way kernel cache, which stores cached versions of drivers, is handled. You now have to update it after replacing any drivers. You can do so by running “kextcache -f -u”.


# macOS 10.12 Sierra

## What Macs became unsupported with this version’s release?

- iMac7,1
- iMac8,1
- iMac9,1
- MacBook5,1
- MacBook5,2
- MacBookAir2,1
- MacBookPro4,1
- MacBookPro5,1
- MacBookPro5,2
- MacBookPro5,3
- MacBookPro5,4
- MacBookPro5,5
- Macmini3,1
- MacPro3,1
- MacPro4,1
- Xserve2,1
- Xserve3,1

## What Macs are still unsupported (but supported by patchers)?

- MacBook4,1

## What projects can I use/browse for more information?
dosdude1 released macOS Sierra Patcher, a simple to use app for running Sierra on unsupported Macs.

I released a tool of my own, macOS Patcher, which uses a command-line interface instead of an app, but works in a similar way. It also supports the MacBook4,1, but without graphics acceleration.

## What new patches do I have to use for this version?
You have to replace these with older versions from 10.11.6:
- AmbientLightSensorHID.plugin (in /System/Library/Extensions/AppleSMCLMU.kext/Contents/PlugIns)
- AppleAirPortBrcm43224.kext (in /System/Library/Extensions)
- AppleHDA.kext (in /System/Library/Extensions)
- IOAudioFamily.kext (in /System/Library/Extensions)

You have to add these custom files from parrotgeek1:
- LegacyUSBEthernet.kext (in /System/Library/Extensions)
- LegacyUSBInjector.kext (in /System/Library/Extensions)
- LegacyUSBVideoSupport.kext (in /System/Library/Extensions)
- SIPManager.kext (in /System/Library/Extensions)
- Trackpad.prefPane (in /System/Library/PreferencePanes)

The Trackpad.prefPane file is created by replacing these files:
- BTTrackpadManager.nib (in the .lproj folders in Contents/Resources)
- MTTrackpadController.nib (in the .lproj folders in Contents/Resources)
- NoTrackpad.nib (in the .lproj folders in Contents/Resources)
- Trackpad.nib (in the .lproj folders in Contents/Resources)

With this file:
- OldTrackpadTab.nib (in the .lproj folders in Contents/Resources)

You have to add these custom files from Czo:
- SUVMMFaker.dylib (in /usr/lib)
- com.apple.softwareupdated.plist (in /System/Library/LaunchDaemons)

If you're using a MacBook4,1, you have to replace these with older versions from 10.11.6:
- AppleHSSPIHIDDriver.kext (in /System/Library/Extensions)
- AppleMultitouchDriver.kext (in /System/Library/Extensions/)

If you have an iMac7,1, iMac8,1, MacBook4,1, MacBookAir2,1, MacBookPro4,1, Macmini3,1, or MacPro3,1, you have to replace these with older versions from 10.11.6:
- corecapture.kext (in /System/Library/Extensions)
- CoreCaptureResponder.kext (in /System/Library/Extensions) 
- IO80211Family.kext (in /System/Library/Extensions)

## What new steps do I have to use for this version?
Sierra updated the command for updating the kernel cache, which stores cached versions of drivers. You now have to update it by running “kextcache -i” or “kextcache -u”.


# macOS 10.13 High Sierra

## The APFS firmware problem
High Sierra introduced a new Apple File System (APFS) optimised for Solid State Drives (SSDs). Supported Macs can use it through a firmware update which was bundled with the High Sierra update. Unsupported Macs can’t boot from APFS volumes since they didn’t receive the firmware update.

You can work around this by using an EFI shell script, which loads the stock APFS driver, and boots from the predefined volumes. To do this you’ll have to copy two files into the EFI partition along with the apfs.efi file from /usr/standalone/i386/apfs.efi (this should be dynamically copied by your patch tool)

## What projects can I use/browse for more information?
dosdude1 released macOS High Sierra Patcher, a simple to use app for running High Sierra on unsupported Macs.

I released a tool of my own, macOS Patcher, which uses a command-line interface instead of an app, but works in a similar way. It also supports the MacBook4,1, but without graphics acceleration.

## What new patches do I have to use for this version?
You have to replace these with older versions from 10.12.6:
- DisplayServices.framework (in /System/Library/PrivateFrameworks)
- AMDRadeonX3000.kext (in /System/Library/Extensions)
- AMDRadeonX3000GLDriver.bundle (in /System/Library/Extensions)
- AMDRadeonX4000.kext (in /System/Library/Extensions)
- AMDRadeonX4000GLDriver.bundle (in /System/Library/Extensions)
- AppleBacklight.kext (in /System/Library/Extensions)
- AppleBacklightExpert.kext (in /System/Library/Extensions)
- AppleUSBTopCase.kext (in /System/Library/Extensions)
- IOAccelerator2D.plugin (in /System/Library/Extensions)
- IOAcceleratorFamily2.kext (in /System/Library/Extensions)

## What new steps do I have to use for this version?
The PlatformSupport.plist file has to be deleted from the Preboot volume too. Your patch tool should find the UUID of your system volume, then mount the Preboot volume and delete this file: /UUID/System/Library/CoreServices/PlatformSupport.plist


# macOS 10.14 Mojave

## What Macs became unsupported with this version’s release?

- iMac10,1
- iMac10,2
- iMac11,1
- iMac11,2
- iMac11,3
- iMac12,1
- iMac12,2
- MacBook6,1
- MacBook7,1
- MacBookAir3,1
- MacBookAir3,2
- MacBookAir4,1
- MacBookAir4,2
- MacBookPro6,1
- MacBookPro6,2
- MacBookPro7,1
- MacBookPro8,1
- MacBookPro8,2
- MacBookPro8,3
- Macmini4,1
- Macmini5,1
- Macmini5,2
- Macmini5,3
- MacPro4,1

## What Macs are still unsupported (but supported by patchers)?

- iMac7,1
- iMac8,1
- iMac9,1
- MacBook4,1
- MacBook5,1
- MacBook5,2
- MacBookAir2,1
- MacBookPro4,1
- MacBookPro5,1
- MacBookPro5,2
- MacBookPro5,3
- MacBookPro5,4
- MacBookPro5,5
- Macmini3,1
- MacPro3,1
- MacPro4,1
- Xserve2,1
- Xserve3,1

## What projects can I use/browse for more information?
dosdude1 released macOS Mojave Patcher, a simple to use app for running Mojave on unsupported Macs.

I released a tool of my own, macOS Patcher, which uses a command-line interface instead of an app, but works in a similar way. It also supports the MacBook4,1, but without graphics acceleration.

## What new patches do I have to use for this version?
You have to replace these with older versions from 10.13.6:
- AirPortAtheros40.kext (in /System/Library/Extensions) (in /System/Library/Extensions/IO80211Family.kext/Contents/PlugIns)
- AMD2400Controller.kext (in /System/Library/Extensions)
- AMD2600Controller.kext (in /System/Library/Extensions)
- AMD3800Controller.kext (in /System/Library/Extensions)
- AMD4600Controller.kext (in /System/Library/Extensions)
- AMD4800Controller.kext (in /System/Library/Extensions)
- AMD5000Controller.kext (in /System/Library/Extensions)
- AMD6000Controller.kext (in /System/Library/Extensions)
- AMDFramebuffer.kext (in /System/Library/Extensions)
- AMDLegacyFramebuffer.kext (in /System/Library/Extensions)
- AMDLegacySupport.kext (in /System/Library/Extensions)
- AMDRadeonVADriver.bundle (in /System/Library/Extensions)
- AMDRadeonVADriver2.bundle (in /System/Library/Extensions)
- AMDRadeonX4000HWServices.kext (in /System/Library/Extensions)
- AMDShared.bundle (in /System/Library/Extensions)
- AMDSupport.kext (in /System/Library/Extensions)
- AppleGraphicsControl.kext (in /System/Library/Extensions)
- AppleGraphicsPowerManagement.kext (in /System/Library/Extensions)
- AppleGVA.framework (in /System/Library/PrivateFrameworks)
- AppleIntelFramebufferAzul.kext (in /System/Library/Extensions)
- AppleIntelFramebufferCapri.kext (in /System/Library/Extensions)
- AppleIntelHD3000Graphics.kext (in /System/Library/Extensions)
- AppleIntelHD3000GraphicsGA.plugin (in /System/Library/Extensions)
- AppleIntelHD3000GraphicsGLDriver.bundle (in /System/Library/Extensions)
- AppleIntelHD3000GraphicsVADriver.bundle (in /System/Library/Extensions)
- AppleIntelHDGraphics.kext (in /System/Library/Extensions)
- AppleIntelHDGraphicsFB.kext (in /System/Library/Extensions)
- AppleIntelHDGraphicsGA.plugin (in /System/Library/Extensions)
- AppleIntelHDGraphicsGLDriver.bundle (in /System/Library/Extensions)
- AppleIntelHDGraphicsVADriver.bundle (in /System/Library/Extensions)
- AppleIntelSNBGraphicsFB.kext (in /System/Library/Extensions)
- AppleIntelSNBVA.bundle (in /System/Library/Extensions)
- AppleMCCSControl.kext (in /System/Library/Extensions)
- AppleUSBACM.kext (in /System/Library/Extensions)
- ATIRadeonX2000.kext (in /System/Library/Extensions)
- ATIRadeonX2000GA.plugin (in /System/Library/Extensions)
- ATIRadeonX2000GLDriver.bundle (in /System/Library/Extensions)
- ATIRadeonX2000VADriver.bundle (in /System/Library/Extensions)
- GeForceTeslaGLDriver.bundle (in /System/Library/Extensions)
- GeForceTeslaVADriver.bundle (in /System/Library/Extensions)
- GPUWrangler.framework (in /System/Library/PrivateFrameworks)
- IOGraphicsFamily.kext (in /System/Library/Extensions)
- IONDRVSupport.kext (in /System/Library/Extensions)
- IOUSBFamily.kext (in /System/Library/Extensions)
- IOUSBHostFamily.kext (in /System/Library/Extensions)
- NVDANV50HalTesla.kext (in /System/Library/Extensions)
- NVDAResmanTesla.kext (in /System/Library/Extensions)

You have to replace these with older versions from 10.14.3:
- libGPUSupport.dylib (in /System/Library/PrivateFrameworks/GPUSupport.framework/Versions/A/Libraries)
- OpenGL.framework (in /System/Library/Frameworks)
- SiriUI.framework (in /System/Library/PrivateFrameworks)

You have to replace this one with an older version from 10.14.4:
- CoreDisplay.framework (in /System/Library/Frameworks)

You have to add these custom files from Syncretic:
- AAAMouSSE.kext (in /System/Library/Extensions)
- telemetrap.kext (in /System/Library/Extensions)

MouSSE is a partial SSE4.2 emulator that allows the MacPro3,1 to use the newer AMD drivers.

telemetrap is a blocker for com.apple.telemetry.plugin in /System/Library/UserEventPlugins that allows Macs with Intel Core 2 Duo CPUs to boot.

You have to replace this one with a modified file from ASentientBot:
- SkyLight.framework (in /System/Library/PrivateFrameworks)

This file is created by modifying the SkyLight or SkyLightOriginal binary in Contents/MacOS for macOS 10.15 Catalina as follows:
```
0x216c60: NOP
```

You have to replace this one with a modified file from ASentientBot:
- GeForceTesla.kext (in /System/Library/Extensions)

This file is created by modifying the GeForceTesla binary in Contents/MacOS by *NOP*ing the following call:
```
nvVirtualAddressSpaceTesla::free() + 183
```

# macOS 10.15 Catalina

## What Macs became unsupported with this version’s release?

- MacPro5,1

## The non-Metal acceleration problem
With the release of Mojave, support for Macs without Apple’s Metal graphics framework was dropped. This wasn’t too much of a problem since older versions of Mojave drivers and frameworks were all that was required to patch it. However, Catalina introduced some more changes to two of those, so they couldn’t be replaced.

ASentientBot created wrappers to port the Mojave versions of CoreDisplay and Skylight to Catalina. The wrappers are stable enough to be used on a daily driver but they’re more of a workaround, than a proper solution, and have to be updated for most new Catalina releases.

## The Library Validation problem
The acceleration wrappers unfortunately trip one of Apple’s security systems: Library Validation. This is due to WindowServer, conflicting with the SkyLight wrapper, which replaces the original binary.

You can disable Library Validation by running “sudo defaults write /Library/Preferences/com.apple.security.libraryvalidation.plist DisableLibraryValidation -bool true”. This requires System Integrity Protection (SIP) to be disabled as well, which can be done in two ways: a patched boot.efi file (in /System/Library/CoreServices) or by running “csrutil disable”.

ASentientBot created this patched boot.efi file by modifiying the boot.efi from the final release of macOS 10.15.4 Catalina. The assembly is as follows:
```
0x1b842: mov dword[rcx+0x498],0xffffffff
or byte[rcx+0x6],0x8
ret
```

## The modern updates problem
Catalina added a new read-only system volume which only works with Apple’s modern updates. Before installing an update, you need to set `-no_compat_check` in your nvram boot arguments, using the following command:
```
if [[ ! "$(nvram boot-args | sed 's/boot-args	//')" == *-no_compat_check ]]; then
	nvram boot-args="$(nvram boot-args | sed 's/boot-args	//') -no_compat_check"
fi
```

For Macs that don't support APFS, you have to use an EFI shell script, which loads the stock APFS driver, and boots from the predefined update volume, then the predefined volume if the update volume isn't found. To do this you’ll have to copy two files into the EFI partition along with the apfs.efi file from /usr/standalone/i386/apfs.efi (this should be dynamically copied by your patch tool)

I patched an update binary to allow us to set the boot argument and copying the EFI shell script designed for updates. This is more of a workaround, than a proper solution, and might have to be updated for new Catalina releases.

The patched atomicupdatetool binary only replaces `/usr/sbin/nvram` with `/sbin/swuinstal` and `/sbin/reboot` with `/sbin/swuble`. This patch can be made by using a hex editor, such as the open source Hex Fiend app on macOS.

I use also another script to replace the EFI shell script designed for updates with the one that the patch tool installs, but only if the system version is newer than the previous system version. This is to decrease the time it takes to boot, as the EFI shell script doesn't need to check for the predefined update volume first.

## What projects can I use/browse for more information?
dosdude1 released macOS Catalina Patcher, a simple to use app for running Catalina on unsupported Macs.

I released a tool of my own, macOS Patcher, which uses a command-line interface instead of an app, but works in a similar way. It also supports the MacBook4,1, but without graphics acceleration.

## What new patches do I have to use for this version?
You have to replace this one with an older version from 10.12.6:
- iSightAudio.driver (in /Library/Audio/Plug-Ins/HAL)

You have to replace these with older versions from 10.14.6:
- AppleIntelPIIXATA.kext (in /System/Library/Extensions)
- AppleYukon2.kext (in /System/Library/Extensions)
- IO80211Family.kext (in /System/Library/Extensions)
- JapaneseIM.app (in /System/Library/Input Methods)
- libmecabra.dylib (in /usr/lib)
- nvenet.kext (in /System/Library/Extensions)
- TextInputCore.framework (in /System/Library/PrivateFrameworks)

You have to replace these with older versions from 10.15.3:
- MonitorPanel.framework (in /System/Library/PrivateFrameworks)
- MonitorPanels (in /System/Library)

You have to replace this one with an older version from 10.15 beta 6:
- libCoreFSCache.dylib (in /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries)

You have to replace this one with a modified file from ASentientBot:
- GeForceTesla.kext (in /System/Library/Extensions)

This file is created by modifying the GeForceTesla binary in Contents/MacOS by *NOP*ing the following call:
```
nvVirtualAddressSpaceTesla::free() + 183
```

And the following addresses:
```
0x5527
0x77993
```

## What new steps do I have to use for this version?
The acceleration wrappers conflict with the dyld cache, which stores cached versions of dylibs. If you have these acceleration wrappers on your system, you now have to update the dyld cache after replacing CoreDisplay and SkyLight. You can do so by running “update_dyld_shared_cache”.

The patched boot.efi file for disabling Library Validation has to be replaced in the Preboot volume too. Your patch tool should find the UUID of your system volume, then mount the Preboot volume and replace this file: /UUID/System/Library/CoreServices/boot.efi

## Catalina Unus
Catalina Unus is a command line tool for running macOS Catalina on one HFS or APFS volume. It does this by using the BaseSystem.dmg file from Mojave, and making some changes to allow installing using this method and to resemble a Catalina installer. It then copies the template for the Catalina data volume to the system, and makes sure all data in the Recovered Files folder is moved back to the system.

You have to remove this folder from the data volume template before copying it to the system:
- dslocal (in /System/Library/Templates/Data/private/var/db)

You have to copy this folder to the system:
- Data (in /System/Library/Templates) to /

You have to copy the data in this folder to the system:
- Recovered Items/ (in /) to /

You can do this using the following commands:
```
for folder in /Recovered\ Items/*; do
	/System/Library/PrivateFrameworks/PackageKit.framework/Versions/A/Resources/shove "$folder" /${folder#/Recovered\ Items/}
done

rm -r /Recovered\ Items

rm -r /System/Library/Templates/Data/private/var/db/dslocal

ditto /System/Library/Templates/Data /
```

