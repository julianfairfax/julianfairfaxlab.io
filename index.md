---
layout: default
title: Home
---

I am a developer and a supporter of the free and open source software movement, a defender of human rights, of democracy, and of European unity.

Je suis un développeur et un défenseur du mouvement des logiciels libres, un défenseur des droits de l'homme, de la démocratie, et de l'unité européenne.

Ich bin ein Entwickler und ein Unterstützer der Bewegung für freie und quelloffene Software, ein Verteidiger der Menschenrechte, der Demokratie, und der europäischen Einigung.

I work on the [Mobian](https://mobian.org) project. I maintain support for the PineTab2 and PineTab, and I have a PinePhone Pro and PinePhone.
