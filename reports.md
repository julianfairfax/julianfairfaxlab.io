---
title: Reports
layout: default
---

## [Wie der Klimawandel von einer Tatsache zu einer politischen Debatte wurde](/reports/Wie_der_Klimawandel_von_einer_Tatsache_zu_einer_politischen_Debatte_wurde.pdf)

Maturaarbeit, die ich Rahmen eines Austauschjahres in Deutschland geschrieben habe.

---

## [Comment le changement climatique est passé d’un fait à un débat politique](/reports/Comment_le_changement_climatique_est_passé_d’un_fait_à_un_débat_politique.pdf)

Travail de maturité que j'ai écrit pendant une année d'échange en Allemagne  moi-même.

---

## [How climate change went from a fact to a political debate](/reports/How_climate_change_went_from_a_fact_to_a_political_debate.pdf)

Work that I wrote during an exchange year in Germany and translated into English myself.
