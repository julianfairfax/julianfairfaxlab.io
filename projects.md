---
layout: default
title: Projects
---

Code

  - Package Repo, [GitLab repo](https://gitlab.com/julianfairfax/package-repo), [Codeberg repo](https://codeberg.org/julianfairfax/package-repo), a repository for installing my projects and others more easily, [how to use](https://gitlab.com/julianfairfax/package-repo#how-to-use)
  - proton-bridge-prebuilt, [GitLab repo](https://gitlab.com/julianfairfax/proton-bridge-prebuilt), [Codeberg repo](https://codeberg.org/julianfairfax/proton-bridge-prebuilt), prebuilt binaries for [proton-bridge](https://github.com/ProtonMail/proton-bridge)
  - proton-ie-prebuilt, [GitLab repo](https://gitlab.com/julianfairfax/proton-ie-prebuilt), [Codeberg repo](https://codeberg.org/julianfairfax/proton-ie-prebuilt), prebuilt binaries for [proton-ie](https://github.com/ProtonMail/proton-bridge/tree/ie-1.3.3)
  - proton-mail-export-prebuilt, [GitLab repo](https://gitlab.com/julianfairfax/proton-mail-export-prebuilt), [Codeberg repo](https://codeberg.org/julianfairfax/proton-mail-export-prebuilt), prebuilt binaries for [proton-mail-export](https://github.com/ProtonMail/proton-mail-export)
  - u-boot-build, [GitLab repo](https://gitlab.com/julianfairfax/u-boot-build), [Codeberg repo](https://codeberg.org/julianfairfax/u-boot-build), GitLab CI workflows to build u-boot for a few Rockchip boards
  - [Signal](https://github.com/signalapp/Signal-Desktop) [Flatpak](https://github.com/signalflatpak/signal)
  - Website, [GitLab repo](https://gitlab.com/julianfairfax/julianfairfax.gitlab.io), [Codeberg repo](https://codeberg.org/julianfairfax/pages), my website
