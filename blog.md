---
title: Blog
layout: default
---

{% for post in site.posts %}
<h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
<h3>{{ post.description }}</h3>
<p>{{ post.excerpt }}</p>
<hr>
{% endfor %}
