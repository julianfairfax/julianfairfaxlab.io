---
title: Contact
layout: default
---

You can send encrypted emails to [my email address](mailto:sendjulianaletter@pm.me) from an encrypted email provider or by using [my public key](/publickey.asc) (you can also send unencrypted emails).
