---
title: Firmware
description: 4 July 2023
layout: default
---

Some notes about what firmware is, where it runs, and where it's stored.

## General information:
From [What is firmware? - Firmware - Debian Wiki](https://wiki.debian.org/Firmware#What_is_firmware.3F):

> Firmware refers to embedded software which controls electronic devices. Well-defined boundaries between firmware and software do not exist, as both terms cover some of the same code. Typically, the term firmware deals with low-level operations in a device, without which the device would be completely non-functional.

### Source code
From [What is firmware? - Firmware - Debian Wiki](https://wiki.debian.org/Firmware#What_is_firmware.3F):

> [A few](https://wiki.debian.org/Firmware/Open) firmware images are Free Software and Open Source but unfortunately almost all of them are non-free, which means that Debian cannot include them as normal in the archive under main or contrib.

Most firmware is non-free. This notably includes Wi-Fi card firmware, which could be a potential privacy risk.

It's almost impossible to create open source firmware for components whose manufacturers don't release open source firmware.

However, efforts surrounding bootloader firmware have proved to be successful, on a small scale. Some amd64 devices are supported by Coreboot, an open source implementation of amd64 bootloader firmware.

In addition to this, U-Boot, an open source bootloader for arm64 devices, has been very successful, and is now used on a large amount of these devices, with it being included by manufacturers.

#### Special cases
Some devices have firmware that is largely open source, or largely non-free, with some non-free or open source components respectively.

A notable example of this is firmware for the PineBuds Pro, which is largely open source, but contains non-free code for the active noise cancellation functionality.

Some devices have firmware that is fully open source, such as the PineTime, an open source smartwatch.

## Firmware on hardware
From [What is firmware? - Firmware - Debian Wiki](https://wiki.debian.org/Firmware#What_is_firmware.3F):

> Many devices require firmware to operate. Historically, firmware would be built into the device's ROM or Flash memory, but more and more often, a firmware image has to be loaded into the device RAM by a device driver during device initialisation. 

Some firmware is still installed to a hardware component directly. This is notably the case with bootloaders, on amd64 devices.

It's worth noting that on an amd64 devices, bootloaders are also pieces of software that are installed as part of the operating system, and are used to boot said operating system. These are not firmware, and you cannot install them to your hardware.

On arm64 devices, bootloaders can also be installed to a device's EMMC or an SD card. It's also possible to install a bootloader to an arm64 device's SPI, which is more or less how bootloaders are installed on amd64 devices.

Firmware stored on hardware components is not as easy to update, but, the storing of bootloader firmware on hardware makes sense, as a computer cannot boot without it.

## Firmware on software
Most firmware is stored on software. This allows components to request their firmware from the operating system, which makes their manufacturing easier, and allows the user to easily keep this firmware up-to-date.

This firmware is stored on the operating system, but runs on the hardware in question.

Microcode is a notable example of this. It's firmware that the CPU runs, that includes fixes for security issues on the CPU's built-in firmware.

### Linux
On Linux, most firmware can be found in the `linux-firmware` repository. This is maintained by the maintainers of the Linux kernel, and distributions are able to package the firmware within it easily.

The `linux-firmware` repository contains both open source and non-free firmware, though most of it is non-free. This means distributions have to choose how to package it.

On most distributions, a single `linux-firmware` package contains all the firmware from the repository, without regard for the license of said firmware.

On Debian, which is committed to software freedom, there exists a `firmware-linux-free` package, containing only the open source firmware from the repository, and a `firmware-linux-nonfree` package that contains the non-free firmware.

#### Special cases
Some firmware doesn't allow redistribution. Because of this, you won't find it in `linux-firwmare`, and will have to download it from a separate package.

These packages make use of the fact they technically aren't redistributing the firmware, but are providing a package that will download the firmware from the original source as part of its installation process.

Sometimes, firmware has not yet made its way to the `linux-firmware` repository.

For this firmware, you can find it in separate packages on most distributions.

### Android
Firmware is stored on the EMMC only, not the hardware.

The `bootloader` partition contains the bootloader. There is open source and non-free firmware on this partition.

The `radio` partition contains the firmware for your modem, which is non-free.

The `product`, `system`, and `system_ext` partitions additionally contain some more non-free firmware.

The `vendor` partition contains most of the non-free firmware required for your device's components.

For custom operating systems, the contents of the `bootloader` and `radio` partitions are copied from the stock operating system.

For the `product`, `system`, `system_ext`, and `vendor` partitions, the open source software is built from its source, and the non-free firmware is copied from the stock operating system.

## Special cases
The PinePhone and PinePhone Pro have a unique modem implementation: their modem runs inside a fully separate operating system.

Because of this, their modem "firmware" is essentially software, which does however contain firmware, for the functionality of the actual modem.

This is an interesting situation, which looks something like this:

- PinePhone and PinePhone Pro
  - Software
    - Firmware on software
  - Firmware on hardware
  - Modem
    - Software ("modem firmware")
      - Firmware on software (firmware for modem)

The stock "modem firmware" is non-free, or at least, not as open source as it could be.

There exists open source "firmware", which replaces the non-free software part of the "firmware" with open source software. This corresponds to the contents of the `boot`, `recovery`, `recoveryfs`, and `system` partitions.

The `aboot` partition contains an open source bootloader in both the stock and the open source "firmware".

The `modem` partition contains the actual modem firmware, which is non-free.

In both the stock and the open source "firmware", the `SBL`, `rpm`, and `tz` partitions also contain non-free firmware.

The end result is a "firmware" that is more open than the stock firmware, and more open than modem firmware on Android devices, though this is partly due to the fact that the operating system running on the modem doesn't need to include the same non-free firmware that the Android operating system does when running on an actual device.
