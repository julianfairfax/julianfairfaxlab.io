---
title: Review of the PineTime
description: 17 December 2023
layout: default
---

A few weeks ago, I got a PineTime. If you don't know what a PineTime is, it's a smartwatch made by Pine64, that runs on open source firmware. That means you can download the [source code](https://github.com/InfiniTimeOrg/InfiniTime) of the firmware, modify it, and flash it to your smartwatch.

I think this is really cool, since we hear a lot about open source operating systems like Linux and Android in the laptop, tablet, and phone space, but we rarely think about the other devices we use, and what they run.

The PineTime uses Bluetooth 5.0, is water resistant down to 1 meter for 30 minutes, and has a 180 mAh battery (which Pine64 says should last a week). The screen is an LCD, which unfortunately means it doesn't support an always-on display.

You can pair it with almost any device, and you can then receive notifications from your device on your PineTime. It can track your steps, measure your heart rate, can be used as a stopwatch, be used an alarm clock, set timers, control your paired devices's media playback, show you your next navigation directions, be used as a metronome, etc.

You can also change the screen's brightness, set the watch to silent, or turn notifications off entirely, use it as a flashlight, wake it up by tapping on it, wake it up by raising your wrist, or wake it up by shaking it. You can change the watch faces (to digital, analog, etc.). You can also set it to chime every hour or 30 minutes.

Of course, it also displays the time, date, pairing status, battery level, step count, heart rate, etc. like any normal smartwatch.

To charge the PineTime, you need a device-specific charger. It does plug into any normal USB-A port, but I think it's unfortunate that the charger doesn't have a USB-C port, so you could use any cable with it.

The watch itself is sealed, so you cannot connect it to anything directly. To flash firmware to it, you can do so over Bluetooth.

In terms of repairability, the fact that the watch is sealed means you cannot repair it easily. Your only option is to buy a new one. However, if the charger stops working, you can buy a replacement, directly from Pine64.

How well does the device actually work? Well, as a watch, it works perfectly. I do not really trust the raise wrist to wake enough to not wake the device when I don't want to, so I personally just use the tap to wake feature instead. This means it's a bit harder to use, but I like knowing exactly when my device is on and when it's not.

Apart from that, I don't know how accurate the step counting is, and the watch doesn't recognise periods of activity or anything, so it's really just a count of steps for the day. But, that's enough for me. I don't care much about about how many steps I do, or when, but it's fun just to have the info.

In my experience, the battery also does actually last a week, though this obviously depends on how much you use it, or rather, what you do with it. Just checking the time a few times a day isn't going to use much battery. But actively using it with the screen on will. Thankfully, I haven't been in a situation where I would need the water resistance, though a bit of weather hasn't bothered it.

Notifications are also received and shown correctly, though they don't support emojis, which is a bit annoying when you consider the fact that modern messaging applications use them for reactions.

I haven't use the watch for much else so far, but I trust it can handle stuff like stopwatches relatively well. It is a watch after all.

I also haven't installed any other firmware on it. The firmware it comes with is the best option for it, and I don't personally have the skills or desire to start developing firmware for the watch myself.

Overall, I would definitely recommend the PineTime to those looking for a smartwatch that just does the basics. It's not expensive, and I think it's worth the price it is. But, it obviously won't do a whole lot of stuff. The fact that it's open source is something I find really cool, but that also won't make a difference for most people.

---

For more info on the PineTime, see the [Pine64 site](https://pine64.org/pinetime), [store](https://pine64.com/product/pinetime-smartwatch-sealed), and [wiki](https://wiki.pine64.org/wiki/PineTime).
