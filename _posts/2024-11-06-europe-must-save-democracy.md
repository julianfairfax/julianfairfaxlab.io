---
title: Europe must save democracy 
description: 6 November 2024
layout: default
---

It is more clear now than it ever was before: Europe needs its own military, Europe needs to defend democracy and human rights in a world in which they are increasingly under attack. We must do it because no one else will.

In case you missed the result of the presidential election in the United States, democracy lost, human rights lost, we all lost. But this is only the beginning of the end. The United States have lost, but the rest of the democratic world does not have to.

Since the signing of the North Atlantic Treaty in 1949, Europe has relied on the US for its defence. At the time, Europe had just been through a World War which destroyed their defence capabilities. Relying on the US did not just make sense, it was necessary. The democratic world was saved by the United States.

Now, it is Europe that must save the democratic world. Europe must save democracy and human rights.