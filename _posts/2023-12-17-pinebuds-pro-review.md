---
title: Review of the PineBuds Pro
description: 17 December 2023
layout: default
---

A few months ago, I got the PineBuds Pro. If you don't know what the PineBuds Pro are, they're wireless earbuds made by Pine64, that run on open source firmware. That means you can download the [source code](https://github.com/pine64/OpenPineBuds) of the firmware, modify it, and flash it to your wireless earbuds.

I think this is really cool, since we hear a lot about open source operating systems like Linux and Android in the laptop, tablet, and phone space, but we rarely think about the other devices we use, and what they run.

The PineBuds Pro use Bluetooth 5.2, are water resistant for up to 10 minutes, have a 40 mAh battery in each earbud and an 800 mAh battery in the charging case (which Pine64 says should mean 5 hours of play time and 25 hours with the case).

You can pair them with any device, and then play whatever you want on them, and also use them as a microphone. They also support active noise cancellation, with different modes (off, on, "super" ANC, and "ambient mode"). The latter means you should be able to hear what's going on around you, even while playing something on the earbuds.

You can change the ANC setting, play and pause media, turn the volume up or down, go to the next track or go back to the previous track, answer or reject a call, mute or unmute the microphone, or hang-up a call, by tapping on the earbuds.

Of course, the earbuds will play media, etc. just like any normal earbuds.

To charge the PineBuds Pro, you just need the case. To charge the case, you can use any USB-C cable.

The PineBuds Pro themselves are sealed, so you cannot connect them to anything directly. To flash firmware to them, you can use the case, by connecting it to the your device.

In terms of repairability, the fact that the earbuds are sealed means you can't repair them easily. However, if they stop working, you can buy a replacement one, individually, directly from Pine64. The charging case is also sold separately.

How well do the earbuds actually work? Well, as wireless earphones, they work perfectly. Sometimes they don't want to pair correctly, and using them with their microphone reduces the audio quality a lot. But none of that is anything I haven't had with every other pair of wireless earphones I have ever used.

The noise cancellation also works really well. I don't know how accurate their claims for it are, but that doesn't really matter either. It's also not always easy to the turn on, but again, that's basically going to be an issue for every pair of wireless earphones. These things are just hard to control.

In my experience, I haven't actually had the battery run out on me yet, but that's probably because I haven't actually used them for a really long time, and I always put them back in the case after using them. How long they will last depends on how much you use them. Thankfully, I haven't been in a situation where I would need the water resistance, though a bit of weather hasn't bothered them.

The earbuds play media well, which is what they were designed for. I haven't really done much else with them.

The microphone is probably something I should test more on them, but in my use case, it doesn't really make sense to use it, since my computer already has one, and the people around me will hear what I'm saying either way.

I also haven't installed any other firmware on them. The firmware they comes with is the best option for them, and I don't personally have the skills or desire to start developing firmware for the earbuds myself.

Overall, I would definitely recommend the PineBuds Pro to those looking for a earbuds that just do the basics. They're not expensive, and I think they're worth the price they are. But, they obviously won't do a whole lot of stuff. The fact that they're open source is something I find really cool, but that also won't make a difference for most people.

---

For more info on the PineBuds Pro, see the [Pine64 store](https://pine64.com/product/pinebuds-pro-open-firmware-capable-anc-wireless-earbuds) and [wiki](https://wiki.pine64.org/wiki/PineBuds_Pro).
