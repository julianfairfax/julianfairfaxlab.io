---
title: Send me a letter
description: 23 January 2024
layout: default
---

Back in the day, people used to send each other letters. Some people would even send letters to strangers, who would then become their friends. They were called pen pals.

Nowadays, sending letters isn't really a thing anymore, and neither are pen pals. Over the past decade, social media has become the standard way of talking to people on the internet. But social media isn't the same as a letter.

Most people don't use social media to talk to their friends, or to make new friends by talking to them, but to talk to everyone, and no one, at the same time. It has lost all of the social interaction it is named after.

We do have solutions to this, and they're called messaging services. They're made specifically to talk to people, individually, or as a group. But not everyone has every messaging service, so it is not quite as universal as a letter.

As it turns out, there is actually a digital way of sending people mail, called email. It isn't really used by people to talk to each other nowadays, because it's not as convenient as messaging services.

But what made email so popular when it was first created, was that everyone had an email address, and every email provider can send and receive emails from every other email provider. This is still the case today, as much as we have forgotten about it.

Of course, for those of you who have privacy and security concerns, when email was created, it was not encrypted, and most people don't use an encrypted email provider. But, it is possible to send encrypted emails, even if your provider, or the recipient's provider doesn't support it.

I think it's unfortunate that people don't write to each other as much anymore, and that pen pals aren't really a thing nowadays.

If you want to write to me, you can send encrypted emails to [my email address](mailto:sendjulianaletter@pm.me) from an encrypted email provider or by using [my public key](/publickey.asc) (you can also send unencrypted emails).

---

Je parle aussi le français, donc vous pouvez m'écrire en français aussi. Und ich lerne auch Deutsch, deswegen könnt ihr mir auf Deutsch auch schreiben.
