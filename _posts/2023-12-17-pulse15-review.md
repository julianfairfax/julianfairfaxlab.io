---
title: Review of the Pulse 15 (three years on)
description: 17 December 2023
layout: default
---

Almost three years ago, I got my Pulse 15. It was my first laptop that was actually for Linux. In case you haven't heard of the Pulse 15, it's a laptop made by Tuxedo, who make laptops specifically for Linux.

I didn't have really high expectations from it. It didn't have the best specs, or at least, the base model I bought doesn't. It came with 8 GB of RAM and 256 GB of storage, but, it did come with an AMD Ryzen 7 4800H CPU and AMD Radeon RX Vega 7 Graphics. It has 3 USB-A ports, a USB-C port, an HDMI port, an Ethernet port, and a Micro SD card reader.

The battery is 91 Wh and is removable, but unfortunately doesn't support charging with USB-C. The charger does work well though, and you can connect it to different power cords for different countries. Here's hoping the third gen Pulse 15 will too. I have the first generation, but I'm really happy with it, and plan to keep using it for years.

One of the things I like about the Pulse 15, aside from the fact that it runs Linux really well, is that it's repairable. You can replace the RAM, SSD, and battery. And all the parts are sold directly by Tuxedo on their website. I upgraded the RAM to 16 GB, and unfortunately also had to replace the battery recently. Within two years of purchasing, the device is under warranty though. For the battery, that warranty is one year.

I have also contacted the support team many many times, because something wasn't working perfectly, or because I was worried about something. Each time, they were very helpful and helped me fixed the issue and reassured me.

I'm very happy with my Pulse 15, and would definitely recommend buying the new third generation one, when it comes out. Though, in hindsight, maybe the Pulse 14 would've been a bit easier to carry around, as the Pulse 15 doesn't fit too easily into backpacks.

---

For more info on the Pulse 15, see [Tuxedo's store](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/TUXEDO-Book-Pulse-15-Gen1.tuxedo) (this is a link to the first generation, which is no longer sold)
