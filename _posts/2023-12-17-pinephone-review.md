---
title: Review of the PinePhone (three years on)
description: 17 December 2023
layout: default
---

Almost three years ago, I got a PinePhone. It was my first entry into the mobile Linux community, which I have found very interesting ever since I learned about it. In case you don't already know, the PinePhone is a phone made by Pine64 for running Linux.

I didn't have really high expectations from the PinePhone. It doesn't have very good specs, or at least, the base model I bought doesn't. It comes with 2 GB of RAM, 16 GB of storage, a USB-C charging port, a removable 3000 mAh battery, and a 1440x720 LCD screen.

Apart from the specs though, the software also wasn't ready, and still isn't ready, for normal use. It's really hard to turn an operating system developed for desktops, laptops, and servers, into one that runs on mobile devices.

Not only do you have to develop the apps and desktop environment to work on a small screen, but you also have to develop new apps that simply didn't exist before, such as for making phone calls, or sending text messages.

All of that can be done though, and has been done. You can run Phosh, which is a desktop environment specifically made for mobile devices. In the future, GNOME shell itself will even support mobile devices. To make calls, you can use the appropriately named GNOME Calls, and to send text messages, you can use Chats.

The problem comes when we get to third-party applications. Basically, every Linux app that you would want to run on a mobile Linux device needed to be developed to work well on them.

However, over the past few years, a lot of progress has actually been made on this issue. With libadwaita, the new toolkit all apps developed for GNOME use, mobile compatibility is made a lot easier, and is almost even the default.

Unfortunately though, not all apps will use this toolkit. Notable examples include Element, the most well-known Matrix client, and Signal, the most well-known private messaging app. Both will not run well on a Linux phone. You can use alternative clients, but they're are not as stable as the official clients.

This is the biggest reason I personally haven't switched to mobile Linux yet. I need Signal to work, otherwise I can't communicate with anyone. And unfortunately, developing a third-party Signal client is very hard, especially because Signal doesn't really want anyone to do so, and doesn't help at all. [Sometimes they even break things.](https://fosstodon.org/@julianfairfax/111279142902447631)

The PinePhone's hardware is also very low-resource, which means using a PinePhone itself as your normal phone is not really possible. What's more of a problem though, is that Linux just isn't optimised for phones. With some optimisation work, it could run a lot better even on devices such as the PinePhone.

Overall, I would definitely recommend the PinePhone to those looking for a low-cost open source phone, but only if it fits for use case. And I wouldn't recommend you purchase one now unless you can help with the device's development in some way. It's not ready for normal users, and I don't want anyone to be disappointed.

---

For more info about the PinePhone, see the [Pine64 site](https://pine64.org/pinephone), [store](https://pine64.com/product/pinephone-beta-edition-linux-smartphone), and [wiki](https://wiki.pine64.org/wiki/PinePhone)
