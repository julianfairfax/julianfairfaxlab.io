---
title: Review of the PineTab2
description: 17 December 2023
layout: default
---

A few months ago, I got a PineTab2. You might even have already heard of that, because [I ported Mobian to it](https://fosstodon.org/@julianfairfax/110587262715357807). If not, maybe you're asking yourself what a PineTab2 is. The PineTab2 is a tablet from Pine64 that runs Linux.

I use Linux on my laptop, and my server, so why shouldn't I also use it on my tablet? I have been interested in mobile Linux for years, but unfortunately, the PineTab was out of stock when I wanted to get one. With the PineTab2, that has changed.

Before receiving the PineTab2, I thought it would probably be easier to use Linux on a tablet than on a phone, because it's basically just a smaller computer. Unfortunately, it wasn't as easy as I would have thought.

Of course, with any new device, you will have to make sure you have the right drivers and everything to run an operating system on it. But that's a problem for most laptops, which can run Linux perfectly. However, with the PineTab2, it's a little different.

That's because the PineTab2 is not a laptop, but a mobile device. And mobile devices aren't made for Linux. That means the drivers it needs will have to be added to the Linux kernel, or, you will have to use a device-specific kernel. That's what the Mobian team had to do for the PineTab2.

The biggest problem with this device though, is that the Wi-Fi driver is not in a working order at this point in time. When buying the device, the page did say some features may not work, but said nothing about Wi-Fi, and it still doesn't. I think this is not acceptable, because it misleads users.

You can still use the device with a Wi-Fi adaptor, but that requires having it connected all the time, which is obviously not the best for ease of use. Bluetooth also doesn't work, and won't until the Wi-Fi driver is working first. You can probably use a Bluetooth adaptor, but that would leave you with two adaptors for two of the most basic and important features.

Apart from that, the device doesn't suffer from most of the issues you will encounter running Linux on a mobile phone, but it also doesn't support features most other tablets do either, such as styluses. This is quite explanatory, because the screen doesn't support them, but, as is a recurring theme with mobile Linux, properly optimised software would be able to make styluses work a lot better, on any screen.

The keyboard case works well, but is unfortunately only available in a US layout. This also makes sense when you consider the price of the device, and how Pine64 cannot make a keyboard layout for each country. They simply don't have the resources. It is unfortunate though.

In terms of specs, there are two variants: one with 64 GB of storage and 4 GB of RAM, and one with 128 GB of storage and 8 GB of RAM. Both variants come with two USB-C ports, which are also used for charging. The battery has a capacity of 6000 mAh and is removable. The screen is a 1280x800 LCD screen.

Once you get past the PineTab2's shortcomings, it can do a lot. I use it with Mobian and Phosh, but you can run many different operating systems on it, and use other desktop environments as well. If you want, you can use it as a normal laptop.

One thing to be aware of though, is that not all Linux apps support ARM devices. Notable examples include Element, the most well-known Matrix client, and Signal, the most well-known private messaging app. You can install third-party ARM versions of these, or use alternative clients, but the latter are not as stable as the official clients.

Otherwise, I don't have much more to say about the device. It isn't really ready for use at the moment, but I have high hopes for it, and it is capable of doing everything a low-cost low-resource Linux laptop can, or at least, it will be.

I would definitely recommend the PineTab2 to those looking for an open source tablet, but only if it fits for use case. And I wouldn't recommend you purchase one now unless you can help with the device's development in some way. It's not ready for normal users, and I don't want anyone to be disappointed.

---

For more info on the PineTab2, see the [Pine64 store](https://pine64.com/product/pinetab2-10-1-4gb-64gb-linux-tablet-with-detached-backlit-keyboard) and [wiki](https://wiki.pine64.org/wiki/PineTab2).
