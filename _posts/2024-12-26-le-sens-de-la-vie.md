---
title: Le sens de la vie
description: 26 December 2024
layout: default
---

Je lis Simone de Beauvoir (_Le deuxième sexe_), je lirai Albert Camus (_L'étranger_). Je ne l'aurais jamais fait il y a deux ans, il y a un an.

Je pense à la philosophie (je _philosophe_). Je me vois existentialiste, comme Camus l'était (il se disait plutôt absurdiste, mais c'est un détail).

J'apprécie mes cours de français, mes cours de philosophie. Je vois l'utilité des sciences humaines, je ne suis plus embêté de devoir les étudier, mais heureux d'avoir l'opportunité de le faire.

Il y a même pas un an, il y a même pas quelques mois, je n'étais pas cette personne. Je voyais la raison et la logique comme tout ce qui comptait dans la vie, les sciences dures comme les seules vraies.

Je trouve que cette personne, ce moi d'il y a quelques mois a raison sur une chose: se poser la question "quel est le sens de la vie?" porte un risque, un risque qu'on ne réfléchira plus que à cette question, sans trouver de réponse, car elle en n'a pas qu'une. On devrait vivre plutôt que réfléchir au sens de vivre.

Mais cet argument ne va qu'à un certain point loin. En effet, la question du sens de la vie est sans réponse, mais n'est-ce justement pas une réponse? Selon moi, le sens de la vie est celui qu'on lui donne. Il est propre à chacun-e. Et avec cette (non-)réponse, on peut arrêter de réfléchir à la question, on peut vivre et en trouver le sens de la vie dans notre propre vie à vous. On peut vivre pour ce pourquoi on a envie de vivre.

Personnellement, je vie pour mes ami-e-s et pour laisser un meilleur monde derrière moi que celui dans lequel je suis arrivé. C'est pour cette raison que j'étudie. C'est dans ce but que je travaillerai.

"L'existentialisme est un courant philosophique et littéraire qui considère que l'être humain forme l'essence de sa vie par ses propres actions, celles-ci n'étant pas prédéterminées par des doctrines théologiques, philosophiques ou morales. L'existentialisme considère chaque individu comme un être unique maître de ses actes, de son destin et des valeurs qu'il décide d'adopter." (Wikipedia. [_Existentialisme_](https://fr.wikipedia.org/wiki/Existentialisme).)

Cette philosophie, que je n'aurais pas connue sans avoir lu sur Camus, est celle que je suis, celle avec laquelle je me sens bien. Je ne vie pas pour cette philosophie et je ne dis pas qu'elle est sans défauts. Mais c'est l'expression philosophique de ce que je ressens, de ce que je pense.

Je pense que chacun-e peut trouver dans la philosophie et dans la littéraire un réconfort duquel une bonne partie du monde se prive, estimant les livres comme étant très intellectuels, ou pas assez. J'étais de ce deuxième avis.

Je suis heureux d'avoir changé, d'avoir trouvé ce réconfort, d'avoir trouvé le sens de la philosophie et de la littérature. Je me disais toujours que je ne lisais pas assez. Aujourd'hui, je me le dis encore, mais je me le dirai toujours. On ne peut jamais lire assez.