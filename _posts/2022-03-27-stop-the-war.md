---
title: Stop the war!
description: 27 March 2022
layout: default
---

More than a month ago, Vladimir Putin started his own personal war against the Ukrainian people, and against democracy. I don't need to tell that this is an unacceptable, manufactured tragedy.

The Russian army is by all definitions more powerful than Ukraine's, except one: morale. While they may have a larger army, Russia has not yet won this war.

NATO hasn't done enough, but is doing more and more for Ukraine. Weapons of all kinds have been and will be sent to help Ukraine defeat Russia.

Putin has started a war he cannot and will not win, against Ukraine, and against the free world. He must, and will lose, whenever, and by whatever means that will happen.

To support Ukraine, you can donate directly to the [armed forces of Ukraine](https://bank.gov.ua/en/about/support-the-armed-forces) or for [humanitarian aid](https://bank.gov.ua/en/about/humanitarian-aid-to-ukraine), through the national bank of Ukraine.

Slava Ukraini! 🇺🇦
