---
title: Review of the Raspberry Pi 4
description: 17 December 2023
layout: default
---

A few months ago, I got a Raspberry Pi 4. This wasn't my first Raspberry Pi, but was definitely a big upgrade over the previous generation, that I had had for years. In case you have never heard of them, they're little computers, ideal for servers.

And running a server is exactly what I'm doing with my Raspberry Pi 4. More precisely, I use it to run a Nextcloud server, through the NextcloudPi project. This project installs everything you need to run and manage your Nextcloud server.

The Raspberry Pi doesn't have the best specs, because it is meant to be a little computer that can be used for all sorts of projects. Mine is the variant with 8 GB of RAM, which does mean it is the most powerful Raspberry Pi 4. Nonetheless, Nextcloud isn't fully optimised for running on such hardware, so it won't be really fast.

With that being said, it is definitely fast enough for normal usage, and using a Raspberry Pi allows you to set everything up quickly and easily, without having to buy or build an expensive server. The Raspebery Pi 4 has four USB ports and an Ethernet port, so you can connect easily connect to other devices.

Personally, I have configured mine to be wireless, and am only using a micro SD card for storage. That means all I need to do to get my server up and running is to plug my Raspberry Pi in, which is made even easier by the fact that it uses USB-C for power. Although, the official charger is definitely a nice addition.

Overall, I'd definitely recommend the Raspberry Pi 4 to anyone who wants to experiment with computers and how doesn't need a powerful device to do so, or to those who want to run a server, like I'm doing.

---

For more info on the Raspberry Pi 4, see the [Raspberry Pi site](https://raspberrypi.com/products/raspberry-pi-4-model-b)
