<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta http-equiv="Permissions-Policy" content="interest-cohort=()"></meta>
    <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
    <title>Firmware | Julian's Blog</title>

    <link rel="stylesheet" type="text/css" href="/assets/css/style.css"></link>

    <link rel="alternate" type="application/rss+xml" title="human-readable label" href="https://julianfairfax.ch/feed.xml"></link>
    <link rel="me" href="https://fosstodon.org/@julianfairfax"></link>
  </head>
  <body>
    <header class="navigation">
      <a href="/">Home</a>
      <a href="/projects">Projects</a>
      <a href="/blog.html">Blog</a>
      <a href="/reports.html">Reports</a>
      <a href="/contact.html">Contact</a>
    </header>

    <header class="page-header">
      <h1 class="project-name">Firmware</h1>
      <h2 class="project-tagline">4 July 2023</h2>
    </header>

    <main id="content" class="main-content">
      <p>Some notes about what firmware is, where it runs, and where it’s stored.</p>

<h2 id="general-information">General information:</h2>
<p>From <a href="https://wiki.debian.org/Firmware#What_is_firmware.3F">What is firmware? - Firmware - Debian Wiki</a>:</p>

<blockquote>
  <p>Firmware refers to embedded software which controls electronic devices. Well-defined boundaries between firmware and software do not exist, as both terms cover some of the same code. Typically, the term firmware deals with low-level operations in a device, without which the device would be completely non-functional.</p>
</blockquote>

<h3 id="source-code">Source code</h3>
<p>From <a href="https://wiki.debian.org/Firmware#What_is_firmware.3F">What is firmware? - Firmware - Debian Wiki</a>:</p>

<blockquote>
  <p><a href="https://wiki.debian.org/Firmware/Open">A few</a> firmware images are Free Software and Open Source but unfortunately almost all of them are non-free, which means that Debian cannot include them as normal in the archive under main or contrib.</p>
</blockquote>

<p>Most firmware is non-free. This notably includes Wi-Fi card firmware, which could be a potential privacy risk.</p>

<p>It’s almost impossible to create open source firmware for components whose manufacturers don’t release open source firmware.</p>

<p>However, efforts surrounding bootloader firmware have proved to be successful, on a small scale. Some amd64 devices are supported by Coreboot, an open source implementation of amd64 bootloader firmware.</p>

<p>In addition to this, U-Boot, an open source bootloader for arm64 devices, has been very successful, and is now used on a large amount of these devices, with it being included by manufacturers.</p>

<h4 id="special-cases">Special cases</h4>
<p>Some devices have firmware that is largely open source, or largely non-free, with some non-free or open source components respectively.</p>

<p>A notable example of this is firmware for the PineBuds Pro, which is largely open source, but contains non-free code for the active noise cancellation functionality.</p>

<p>Some devices have firmware that is fully open source, such as the PineTime, an open source smartwatch.</p>

<h2 id="firmware-on-hardware">Firmware on hardware</h2>
<p>From <a href="https://wiki.debian.org/Firmware#What_is_firmware.3F">What is firmware? - Firmware - Debian Wiki</a>:</p>

<blockquote>
  <p>Many devices require firmware to operate. Historically, firmware would be built into the device’s ROM or Flash memory, but more and more often, a firmware image has to be loaded into the device RAM by a device driver during device initialisation.</p>
</blockquote>

<p>Some firmware is still installed to a hardware component directly. This is notably the case with bootloaders, on amd64 devices.</p>

<p>It’s worth noting that on an amd64 devices, bootloaders are also pieces of software that are installed as part of the operating system, and are used to boot said operating system. These are not firmware, and you cannot install them to your hardware.</p>

<p>On arm64 devices, bootloaders can also be installed to a device’s EMMC or an SD card. It’s also possible to install a bootloader to an arm64 device’s SPI, which is more or less how bootloaders are installed on amd64 devices.</p>

<p>Firmware stored on hardware components is not as easy to update, but, the storing of bootloader firmware on hardware makes sense, as a computer cannot boot without it.</p>

<h2 id="firmware-on-software">Firmware on software</h2>
<p>Most firmware is stored on software. This allows components to request their firmware from the operating system, which makes their manufacturing easier, and allows the user to easily keep this firmware up-to-date.</p>

<p>This firmware is stored on the operating system, but runs on the hardware in question.</p>

<p>Microcode is a notable example of this. It’s firmware that the CPU runs, that includes fixes for security issues on the CPU’s built-in firmware.</p>

<h3 id="linux">Linux</h3>
<p>On Linux, most firmware can be found in the <code class="language-plaintext highlighter-rouge">linux-firmware</code> repository. This is maintained by the maintainers of the Linux kernel, and distributions are able to package the firmware within it easily.</p>

<p>The <code class="language-plaintext highlighter-rouge">linux-firmware</code> repository contains both open source and non-free firmware, though most of it is non-free. This means distributions have to choose how to package it.</p>

<p>On most distributions, a single <code class="language-plaintext highlighter-rouge">linux-firmware</code> package contains all the firmware from the repository, without regard for the license of said firmware.</p>

<p>On Debian, which is committed to software freedom, there exists a <code class="language-plaintext highlighter-rouge">firmware-linux-free</code> package, containing only the open source firmware from the repository, and a <code class="language-plaintext highlighter-rouge">firmware-linux-nonfree</code> package that contains the non-free firmware.</p>

<h4 id="special-cases-1">Special cases</h4>
<p>Some firmware doesn’t allow redistribution. Because of this, you won’t find it in <code class="language-plaintext highlighter-rouge">linux-firwmare</code>, and will have to download it from a separate package.</p>

<p>These packages make use of the fact they technically aren’t redistributing the firmware, but are providing a package that will download the firmware from the original source as part of its installation process.</p>

<p>Sometimes, firmware has not yet made its way to the <code class="language-plaintext highlighter-rouge">linux-firmware</code> repository.</p>

<p>For this firmware, you can find it in separate packages on most distributions.</p>

<h3 id="android">Android</h3>
<p>Firmware is stored on the EMMC only, not the hardware.</p>

<p>The <code class="language-plaintext highlighter-rouge">bootloader</code> partition contains the bootloader. There is open source and non-free firmware on this partition.</p>

<p>The <code class="language-plaintext highlighter-rouge">radio</code> partition contains the firmware for your modem, which is non-free.</p>

<p>The <code class="language-plaintext highlighter-rouge">product</code>, <code class="language-plaintext highlighter-rouge">system</code>, and <code class="language-plaintext highlighter-rouge">system_ext</code> partitions additionally contain some more non-free firmware.</p>

<p>The <code class="language-plaintext highlighter-rouge">vendor</code> partition contains most of the non-free firmware required for your device’s components.</p>

<p>For custom operating systems, the contents of the <code class="language-plaintext highlighter-rouge">bootloader</code> and <code class="language-plaintext highlighter-rouge">radio</code> partitions are copied from the stock operating system.</p>

<p>For the <code class="language-plaintext highlighter-rouge">product</code>, <code class="language-plaintext highlighter-rouge">system</code>, <code class="language-plaintext highlighter-rouge">system_ext</code>, and <code class="language-plaintext highlighter-rouge">vendor</code> partitions, the open source software is built from its source, and the non-free firmware is copied from the stock operating system.</p>

<h2 id="special-cases-2">Special cases</h2>
<p>The PinePhone and PinePhone Pro have a unique modem implementation: their modem runs inside a fully separate operating system.</p>

<p>Because of this, their modem “firmware” is essentially software, which does however contain firmware, for the functionality of the actual modem.</p>

<p>This is an interesting situation, which looks something like this:</p>

<ul>
  <li>PinePhone and PinePhone Pro
    <ul>
      <li>Software
        <ul>
          <li>Firmware on software</li>
        </ul>
      </li>
      <li>Firmware on hardware</li>
      <li>Modem
        <ul>
          <li>Software (“modem firmware”)
            <ul>
              <li>Firmware on software (firmware for modem)</li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </li>
</ul>

<p>The stock “modem firmware” is non-free, or at least, not as open source as it could be.</p>

<p>There exists open source “firmware”, which replaces the non-free software part of the “firmware” with open source software. This corresponds to the contents of the <code class="language-plaintext highlighter-rouge">boot</code>, <code class="language-plaintext highlighter-rouge">recovery</code>, <code class="language-plaintext highlighter-rouge">recoveryfs</code>, and <code class="language-plaintext highlighter-rouge">system</code> partitions.</p>

<p>The <code class="language-plaintext highlighter-rouge">aboot</code> partition contains an open source bootloader in both the stock and the open source “firmware”.</p>

<p>The <code class="language-plaintext highlighter-rouge">modem</code> partition contains the actual modem firmware, which is non-free.</p>

<p>In both the stock and the open source “firmware”, the <code class="language-plaintext highlighter-rouge">SBL</code>, <code class="language-plaintext highlighter-rouge">rpm</code>, and <code class="language-plaintext highlighter-rouge">tz</code> partitions also contain non-free firmware.</p>

<p>The end result is a “firmware” that is more open than the stock firmware, and more open than modem firmware on Android devices, though this is partly due to the fact that the operating system running on the modem doesn’t need to include the same non-free firmware that the Android operating system does when running on an actual device.</p>

    </main>
  </body>
</html>
